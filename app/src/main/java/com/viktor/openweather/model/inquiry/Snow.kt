package com.viktor.openweather.model.inquiry

import com.google.gson.annotations.SerializedName

data class Snow (
    @SerializedName("1h")
    private var _1h: Float
)