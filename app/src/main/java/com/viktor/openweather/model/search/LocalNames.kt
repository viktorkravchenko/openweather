package com.viktor.openweather.model.search


import com.google.gson.annotations.SerializedName

data class LocalNames(
    @SerializedName("ar")
    val ar: String,
    @SerializedName("en")
    val en: String,
    @SerializedName("fr")
    val fr: String,
    @SerializedName("ko")
    val ko: String,
    @SerializedName("ru")
    val ru: String,
    @SerializedName("uk")
    val uk: String
)