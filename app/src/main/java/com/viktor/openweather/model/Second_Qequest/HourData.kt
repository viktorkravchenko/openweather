package com.viktor.openweather.model.Second_Qequest

import com.google.gson.annotations.SerializedName

data class HourData (
    @SerializedName("dt")
    var dt: Int,

    @SerializedName("main")
    var main: Main,

    @SerializedName("weather")
    var weather: List<Weather>,

    @SerializedName("clouds")
    var clouds: Clouds,

    @SerializedName("wind")
    var wind: Wind,

    @SerializedName("visibility")
    var visibility: Int,

    @SerializedName("pop")
    var pop: Float,

    @SerializedName("sys")
    var sys: Sys,

    @SerializedName("dt_txt")
    var dtTxt: String,

    @SerializedName("rain")
    var rain: Rain,

    @SerializedName("snow")
    var snow: Snow
    )