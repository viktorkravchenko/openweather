package com.viktor.openweather.model.Second_Qequest

import com.google.gson.annotations.SerializedName

data class Example (
    @SerializedName("cod")
    var cod: String,

    @SerializedName("message")
    var message: Int,

    @SerializedName("cnt")
    var cnt: Int,

    @SerializedName("list")
    var list: List<HourData>,

    @SerializedName("city")
    var city: City
    )