package com.viktor.openweather.model.Second_Qequest

import com.google.gson.annotations.SerializedName

data class Clouds (
    @SerializedName("all")
    var all: Int
)