package com.viktor.openweather.model.Second_Qequest

import com.google.gson.annotations.SerializedName

data class Sys(
    @SerializedName("pod")
    var pod: String
)