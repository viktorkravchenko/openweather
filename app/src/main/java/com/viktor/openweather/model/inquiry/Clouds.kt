package com.viktor.openweather.model.inquiry

import com.google.gson.annotations.SerializedName

data class Clouds (
    @SerializedName("all")
    var all: Int
)