package com.viktor.openweather.di

import com.viktor.openweather.api.RetrofitApi
import com.viktor.openweather.network.Retrofit
import com.viktor.openweather.repository.WeatherRepository
import com.viktor.openweather.repository.WeatherRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit = Retrofit()

    @Provides
    @Singleton
    fun provideRetrofitApi(retrofit: Retrofit): RetrofitApi =
        retrofit.create(RetrofitApi::class.java)

    @Provides
    @Singleton
    fun provideWeatherRepository(retrofitApi: RetrofitApi): WeatherRepository =
        WeatherRepositoryImpl(retrofitApi)


}
