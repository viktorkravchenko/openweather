package com.viktor.openweather.repository

import com.viktor.openweather.model.Second_Qequest.Example
import com.viktor.openweather.model.inquiry.WeatherApi
import com.viktor.openweather.model.modelByDay.ExampleDay
import com.viktor.openweather.model.search.Location

interface WeatherRepository {
    suspend fun getWeatherByName(
        lat: Double,
        lon: Double,
    ): WeatherApi

    suspend fun getWeatherBySearch(
        cityName: String,
    ): List<Location>

    suspend fun getWeatherHour(
        lat: Double,
        lon: Double,
    ): Example

    suspend fun getWeatherDay(
        lat: Double,
        lon: Double,
    ): ExampleDay
}