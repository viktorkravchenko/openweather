package com.viktor.openweather.repository

import com.viktor.openweather.api.RetrofitApi
import com.viktor.openweather.model.Second_Qequest.Example
import com.viktor.openweather.model.inquiry.WeatherApi
import com.viktor.openweather.model.modelByDay.ExampleDay
import com.viktor.openweather.model.search.Location

class WeatherRepositoryImpl(
    private val retrofit: RetrofitApi
) : WeatherRepository {

    override suspend fun getWeatherByName(lat: Double, lon: Double): WeatherApi =
        retrofit.getWeatherByName(
            lon = lon,
            lat = lat,
            token = "0e833f3c0077edb47169cdb85d4693c2",
            units = "metric"
        )

    override suspend fun getWeatherBySearch(cityName: String): List<Location> =
        retrofit.getWeatherBySearch(
            cityName = cityName,
            token = "0e833f3c0077edb47169cdb85d4693c2",
            limit = 5
        )

    override suspend fun getWeatherHour(lat: Double, lon: Double): Example =
        retrofit.getWeatherHour(
            lon = lon,
            lat = lat,
            token = "0e833f3c0077edb47169cdb85d4693c2",
            units = "metric"
        )

    override suspend fun getWeatherDay(lat: Double, lon: Double): ExampleDay =
        retrofit.getWeatherDay(
            lon = lon,
            lat = lat,
            exclude = "current",
            token = "0e833f3c0077edb47169cdb85d4693c2",
            units = "metric"
        )
}