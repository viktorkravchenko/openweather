package com.viktor.openweather.ui.home

import android.util.Log
import androidx.lifecycle.*
import com.viktor.openweather.model.Second_Qequest.City
import com.viktor.openweather.model.Second_Qequest.Example
import com.viktor.openweather.model.inquiry.WeatherApi
import com.viktor.openweather.model.modelByDay.ExampleDay
import com.viktor.openweather.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    private val _liveData = MutableLiveData<WeatherApi>()
    val liveData: LiveData<WeatherApi> = _liveData
    private val _liveData1 = MutableLiveData<Example>()
    val liveData1: LiveData<Example> = _liveData1
    private val _liveData2 = MutableLiveData<ExampleDay>()
    val liveData2: LiveData<ExampleDay> = _liveData2
    fun weatherNow(coordinates_lat: Double,coordinates_lon: Double) {

        viewModelScope.launch {
            _liveData.value = weatherRepository.getWeatherByName(coordinates_lat,coordinates_lon)
        }
    }

    fun hourlyWeather(coordinates_lat: Double,coordinates_lon: Double) {
        viewModelScope.launch {
            _liveData1.value = weatherRepository.getWeatherHour(coordinates_lat,coordinates_lon)
        }

    }

    fun extendedWeather(coordinates_lat: Double,coordinates_lon: Double) {
        viewModelScope.launch {
            _liveData2.value = weatherRepository.getWeatherDay(coordinates_lat, coordinates_lon)

        }

    }

}





