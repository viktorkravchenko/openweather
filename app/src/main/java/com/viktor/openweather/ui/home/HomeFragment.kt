package com.viktor.openweather.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.squareup.picasso.Picasso
import com.viktor.openweather.R
import com.viktor.openweather.databinding.HomeFragmentBinding
import com.viktor.openweather.model.inquiry.WeatherApi
import com.viktor.openweather.ui.home.adapters.DayAdapter
import com.viktor.openweather.ui.home.adapters.HoursAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.home_fragment) {

    private val adapterHours = HoursAdapter()
    private val adapterDay = DayAdapter()
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setFragmentResultListener(KEY_CITY) { _, result ->
            val coordinates_lat = result.getDouble(LAT)
            val coordinates_lon = result.getDouble(LON)
            viewModel.weatherNow(coordinates_lat, coordinates_lon)
            viewModel.hourlyWeather(coordinates_lat, coordinates_lon)
            viewModel.extendedWeather(coordinates_lat, coordinates_lon)
        }
        initView()
        initObservers()
        initListeners()

    }

    private fun initView() {
        val hourRecyclerView = binding.hourRecyclerView
        hourRecyclerView?.adapter = adapterHours
        val dayRecyclerView = binding.dayRecyclrView
        dayRecyclerView?.adapter = adapterDay

    }

    private fun initObservers() {
        viewModel.liveData.observe(viewLifecycleOwner, {
            setData(it)
        })
        viewModel.liveData1.observe(viewLifecycleOwner, {
            adapterHours!!.setData(it.list)
            it.list[0].main.humidity
            binding?.Speed?.text = it.list[0].wind.speed.toString() + " км/ч"
            binding?.Humidity?.text = it.list[0].main.humidity.toString() + "%"
        })
        viewModel.liveData2.observe(viewLifecycleOwner, {
            adapterDay.setData(it.daily)
        })
    }

    private fun setData(data: WeatherApi) {
        val simpleDateFormat = SimpleDateFormat("HH:mm")
        binding?.textTemp?.text = resources.getString(R.string.temp, data.main.temp)
        binding?.feelsLike?.text = resources.getString(R.string.feels_like, data.main.feelsLike)
        Picasso.with(context).load("https://openweathermap.org/img/wn/${data.weather[0].icon}.png")
            .into(binding?.imageWeather)
        binding?.textCity?.text = data.name
        binding?.Sunrise?.text = simpleDateFormat.format(Date(data.sys.sunrise * 1000L))
        binding?.Sunset?.text = simpleDateFormat.format(Date(data.sys.sunset * 1000L))

    }

    private fun initListeners() {
        binding.textCity?.setOnClickListener {
              findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToSearchFragment2())
        }
    }

    companion object {
        const val KEY_CITY = "KEY_CITY"
        const val LAT = "LAT"
        const val LON = "LON"

        @JvmStatic
        fun newInstance(city: String?) = HomeFragment().apply {
            arguments = Bundle().apply {
                putString(KEY_CITY, city)
            }
        }
    }
}