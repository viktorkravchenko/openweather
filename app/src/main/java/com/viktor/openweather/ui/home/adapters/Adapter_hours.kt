package com.viktor.openweather.ui.home.adapters

import androidx.recyclerview.widget.RecyclerView
import com.viktor.openweather.model.Second_Qequest.HourData
import android.view.ViewGroup
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.viktor.openweather.R
import java.text.SimpleDateFormat
import java.util.*

class HoursAdapter : RecyclerView.Adapter<HoursHolder>() {

    private var list = mutableListOf<HourData>()

    fun setData(data: List<HourData>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoursHolder =
        HoursHolder(
            view = LayoutInflater.from(parent.context).inflate(R.layout.items_hours, parent, false),
            context = parent.context
        )

    override fun onBindViewHolder(holder: HoursHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

}


class HoursHolder(
    private val view: View,
    private val context: Context
) : RecyclerView.ViewHolder(view) {

    private var time: TextView = view.findViewById(R.id.timeHours)
    private var image: ImageView = view.findViewById(R.id.imageHours)
    private var temp: TextView = view.findViewById(R.id.tempHours)
    private var humidity: TextView = view.findViewById(R.id.humidityHours)

    fun bind(model: HourData) {
        Picasso.with(context)
            .load("https://openweathermap.org/img/wn/${model.weather.first().icon}.png")
            .into(image)
        time.text = SimpleDateFormat("HH:mm").format(Date(model.dt * 1000L))
        temp.text = model.main.temp.toString()
        humidity.text = "${model.main.humidity} %"
    }

}
