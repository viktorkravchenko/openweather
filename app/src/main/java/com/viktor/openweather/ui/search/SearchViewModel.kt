package com.viktor.openweather.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.viktor.openweather.model.search.Location
import com.viktor.openweather.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
) : ViewModel() {
    private val _liveData = MutableLiveData<List<Location>>()
    val liveData: LiveData<List<Location>> = _liveData

    fun search(city: String) {

            viewModelScope.launch {
                _liveData.value = weatherRepository.getWeatherBySearch(city )
            }

    }
}