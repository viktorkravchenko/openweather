package com.viktor.openweather.ui.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.viktor.openweather.R
import com.viktor.openweather.databinding.ItemListCityBinding
import com.viktor.openweather.model.search.Location

class SearchAdapter(
    private val onClick: (Location) -> Unit
) : RecyclerView.Adapter<CityHolder>() {
    private var list = mutableListOf<Location>()


    fun setData(data: List<Location>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityHolder =
        CityHolder(
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_city, parent, false),
            onClick

        )

    override fun onBindViewHolder(holder: CityHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size
}

class CityHolder(

    private val view: View,
    private val onClick: (Location) -> Unit
) : RecyclerView.ViewHolder(view) {

    fun bind(model: Location) {
        ItemListCityBinding.bind(view).apply {
            itemTextCity.text = model.name
            itemTextCity.setOnClickListener {
                onClick(model)

            }

        }

    }

}