package com.viktor.openweather.ui.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.viktor.openweather.databinding.StartFragmentBinding

class StartFragment : Fragment() {
    private lateinit var binding: StartFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = StartFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        initListeners()
    }

    private fun initListeners() {
        binding.enterCity.setOnClickListener {
            findNavController().navigate(
                StartFragmentDirections.actionStartFragmentToSearchFragment()

            )
        }
    }
}