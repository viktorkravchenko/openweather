package com.viktor.openweather.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.viktor.openweather.R
import com.viktor.openweather.databinding.SearchFragmentBinding
import com.viktor.openweather.ui.home.HomeFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment(R.layout.search_fragment) {
    var inputText: EditText? = null
    private var city: String? = null
    private val viewModel: SearchViewModel by viewModels()
    private lateinit var binding: SearchFragmentBinding

    private val adapterCity = SearchAdapter {
        parentFragmentManager.setFragmentResult(
            HomeFragment.KEY_CITY, bundleOf(
                HomeFragment.LAT to it.lat, HomeFragment.LON to it.lon
            )
        )
        findNavController().navigate(
            SearchFragmentDirections.actionSearchFragmentToHomeFragment2()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SearchFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        initView()
        initListeners()
    }

    private fun initView() {
        inputText = binding.inputext
        val listCity = binding.listCity
        listCity.adapter = adapterCity
    }

    private fun initListeners() {
        binding.inputext.addTextChangedListener {

            city = inputText?.text.toString()
            if (city != "") {
                viewModel.search(city!!)
            }

            viewModel.liveData.observe(viewLifecycleOwner, {
                adapterCity.setData(it)
            })
        }
        binding.clear.setOnClickListener {
            binding.inputext.setText("")

        }
        binding.back.setOnClickListener {
           findNavController().popBackStack()
        }
    }

    companion object {
        private const val KEY_CITY = "KEY_CITY"

        @JvmStatic
        fun newInstance(city: String?) = HomeFragment().apply {
            arguments = Bundle().apply {
                putString(KEY_CITY, city)
            }
        }
    }
}