package com.viktor.openweather.ui.home.adapters


import com.viktor.openweather.model.modelByDay.Daily
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.viktor.openweather.R
import java.text.SimpleDateFormat
import java.util.*

class DayAdapter() : RecyclerView.Adapter<DayHolder>() {

    var list = mutableListOf<Daily>()

    fun setData(data: List<Daily>?) {
        list.clear()
        list.addAll(data!!)
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayHolder =
        DayHolder(
            view = LayoutInflater.from(parent.context).inflate(R.layout.items_day, parent, false),
            context = parent.context

        )

    override fun onBindViewHolder(holder: DayHolder, position: Int) {

        holder.bind(list[position])

    }

    override fun getItemCount(): Int = list.size

}

class DayHolder(
    private val view: View,
    private val context: Context
) : RecyclerView.ViewHolder(view) {

    private var timeDay: TextView = view.findViewById(R.id.timeDay)
    private var imageDay: ImageView = view.findViewById(R.id.imageDay)
    private var tempDay: TextView = view.findViewById(R.id.tempDay)
    private var humidityDay: TextView = view.findViewById(R.id.humidityDay)

    fun bind(model: Daily) {

        Picasso.with(context)
            .load("https://openweathermap.org/img/wn/${model.weather.first().icon}.png")
            .into(imageDay)
        timeDay.text = SimpleDateFormat("EEEE").format(Date(model.dt * 1000L))
        tempDay.text = model.temp.day.toString()
        humidityDay.text = "${model.humidity} %"

    }
}