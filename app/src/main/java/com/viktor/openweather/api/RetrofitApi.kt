package com.viktor.openweather.api

import retrofit2.http.GET
import com.viktor.openweather.model.Second_Qequest.Example
import com.viktor.openweather.model.inquiry.WeatherApi
import com.viktor.openweather.model.modelByDay.ExampleDay
import com.viktor.openweather.model.search.Location
import retrofit2.http.Query

interface RetrofitApi {
    @GET("/data/2.5/weather?")
    suspend fun getWeatherByName(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") token: String,
        @Query("units") units: String,
    ): WeatherApi

    @GET("/geo/1.0/direct?")
    suspend fun getWeatherBySearch(
        @Query("q") cityName: String,
        @Query("appid") token: String,
        @Query("limit") limit: Int,
    ): List<Location>

    @GET("/data/2.5/forecast?")
    suspend fun getWeatherHour(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") token: String,
        @Query("units") units: String,
    ): Example

    @GET("/data/2.5/onecall?")
    suspend fun getWeatherDay(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String,
        @Query("appid") token: String,
        @Query("units") units: String,
    ): ExampleDay
}